const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

document.write("<h1>Kata 3</h1>");

document.write("<u>" + "Display the numbers from 1 to 20." + "</u>" + "<br><br>");

for (let num = 1; num <= 20; num++){
    document.write(num + " ");
}

document.write( "<br><br>" + "<u>" + "Display the even numbers from 1 to 20." + "</u>" + "<br><br>");

for (let counter = 1; counter <=20; counter++){
    if (counter%2!=0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the odd numbers from 1 to 20." + "</u>" + "<br><br>");

for (let counter = 1; counter <=20; counter++){
    if (counter%2===0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the multiples of 5 from 1 to 100." + "</u>" + "<br><br>");

for (let counter = 1; counter <=100; counter++){
    if (counter%5!=0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the square numbers up to 100." + "</u>" + "<br><br>");

for (let counter = 1; counter <=100; counter++){
    let rooted =Math.sqrt(counter);
    if (Number.isInteger(rooted)==false) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the even numbers from 20 to 1." + "</u>" + "<br><br>");

for (let counter = 20; counter >=1; counter--){
    if (counter%2!=0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the odd numbers from 20 to 1." + "</u>" + "<br><br>");

for (let counter = 20; counter >=1; counter--){
    if (counter%2===0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the multiples of 5 from 100 to 1." + "</u>" + "<br><br>");

for (let counter = 100; counter >=1; counter--){
    if (counter%5!=0) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the square numbers down from 100 to 1." + "</u>" + "<br><br>");

for (let counter = 100; counter >=1; counter--){
    let rooted =Math.sqrt(counter);
    if (Number.isInteger(rooted)==false) continue;
    document.write(counter + " ");
}

document.write( "<br><br>" + "<u>" + "Display the 20 elements of sampleArray." + "</u>" + "<br><br>");

for(let counter = 0; counter<sampleArray.length; counter++){
  document.write(sampleArray[counter] + " ");
}

document.write( "<br><br>" + "<u>" + "Display all the even numbers contained in sampleArray." + "</u>" + "<br><br>");

for(let counter = 0; counter<sampleArray.length; counter++){
    if (sampleArray[counter]%2!=0) continue;
    document.write(sampleArray[counter] + " ");
}

document.write( "<br><br>" + "<u>" + "Display all the odd numbers contained in sampleArray." + "</u>" + "<br><br>");

for(let counter = 0; counter<sampleArray.length; counter++){
    if (sampleArray[counter]%2===0) continue;
    document.write(sampleArray[counter] + " ");
}

document.write( "<br><br>" + "<u>" + "Display the square of each element in sampleArray." + "</u>" + "<br><br>");

function square(x){
      return x*x;
}
for(let counter = 0; counter<sampleArray.length; counter++){
    document.write(square(sampleArray[counter]) + " ");
}

document.write( "<br><br>" + "<u>" + "Display the sum of all the numbers from 1 to 20." + "</u>" + "<br><br>");

let sum = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].reduce(add, 0);

function add(a, b) {
    return a + b;
}

document.write(sum);

document.write( "<br><br>" + "<u>" + "Display the sum of all the elements in sampleArray." + "</u>" + "<br><br>");

let sampleReduce = sampleArray.reduce(add, 0)
document.write(sampleReduce);

document.write( "<br><br>" + "<u>" + "Display the smallest element in sampleArray" + "</u>" + "<br><br>");

let sampleMini = Math.min(...sampleArray)
document.write(sampleMini);

document.write( "<br><br>" + "<u>" + "Display the largest element in sampleArray." + "</u>" + "<br><br>");

let sampleMax = Math.max(...sampleArray)
document.write(sampleMax);

document.write( "<br><br>" + "<u>" + "Display 20 solid gray rectangles, each 20px high and 100px wide." + "</u>" + "<br><br>");

function rect(height, width, color){
    document.write(`<div style=" height: ${height}px; width: ${width}px; background-color: ${color}"></div><br>`);
}

for (let counter=1; counter <= 20; counter++){
    rect(20, 100, 'grey');
}

document.write( "<br><br>" + "<u>" + "Display 20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px." + "</u>" + "<br><br>");

for (let counter=105; counter <= 200; counter+=5){
    rect(20, counter, 'grey');
}

document.write( "<br><br>" + "<u>" + "Display 20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray." + "</u>" + "<br><br>");

sampleArray.forEach(function(item){
    rect(20, item, 'grey')
})

document.write( "<br><br>" + "<u>" + "As in #21, but alternate colors so that every other rectangle is red." + "</u>" + "<br><br>");


for(let i = 0; i <= sampleArray.length; i++){
    if (i % 2 !==0){
        rect(20, sampleArray[i], 'red');
    }
    else{
        rect(20, sampleArray[i], 'grey');
    }
}

document.write( "<br><br>" + "<u>" + "As in #21, but color the rectangles with even widths red." + "</u>" + "<br><br>");

for(let i = 0; i <= sampleArray.length; i++){
    if (sampleArray[i] % 2 ==0){
        rect(20, sampleArray[i], 'red');
    }
    else{
        rect(20, sampleArray[i], 'grey');
    }
}